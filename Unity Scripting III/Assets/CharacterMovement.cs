﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(CharacterController))]

public class CharacterMovement : MonoBehaviour {

    public float Speed = 5f;
    public float JumpHeight = 2f;
    public float Gravity = -9.81f;
    public float DashFactor = 2f;
    public Vector3 Drag;
    public float smoothTime = 0.15f;

    private CharacterController characterController;
    private Vector3 moveDirection;
    private Vector3 smoothMoveDirection;
    private Vector3 smoother;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    public void moveCharacter(float horizontal, float vertical, bool jump, bool dash)
    {
        float deltaTime = Time.deltaTime;
        float dashF = 1f;

        if (characterController.isGrounded)
        {
            moveDirection = horizontal * transform.right + vertical * transform.forward;

            if (dash) dashF = DashFactor;
            if (jump) moveDirection.y = Mathf.Sqrt(JumpHeight * -2f * Gravity);
        }
        // Apply Gravity
        moveDirection.y += (Gravity * deltaTime);

        //Apply Drag
        moveDirection.x /= 1 + Drag.x * deltaTime;
        moveDirection.y /= 1 + Drag.y * deltaTime;
        moveDirection.z /= 1 + Drag.z * deltaTime;

        //Smooth Direction
        smoothMoveDirection = Vector3.SmoothDamp(smoothMoveDirection, moveDirection, ref smoother, smoothTime);

        // Jump is not smoothed
        smoothMoveDirection.y = moveDirection.y;

        // Apply move to the character 
        characterController.Move(smoothMoveDirection * (deltaTime * Speed * dashF));
    }
}
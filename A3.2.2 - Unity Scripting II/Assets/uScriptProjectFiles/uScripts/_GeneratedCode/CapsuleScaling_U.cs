//uScript Generated Code - Build 1.0.3104
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CapsuleScaling_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.Single local_12_System_Single = (float) 0;
   System.Single local_17_System_Single = (float) 0;
   UnityEngine.Vector3 local_24_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_Axes_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_scaleUnits_System_Single = (float) 1;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_23 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_6;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_6;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_6;
   bool logic_uScriptAct_GetDeltaTime_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_9 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_9 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_9 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_9;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_9;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_15 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_15 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_15 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_15;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_15 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_target_1 = new Vector3( );
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_1 = new Vector3( );
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_axis_21 = new Vector3( );
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_angle_21 = (float) 0;
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_23 || false == m_RegisteredForEvents )
      {
         owner_Connection_23 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_9.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_15.SetParent(g);
      owner_Connection_23 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("1538bc80-05de-4fd2-bd0a-684199eca017", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_ClampVector3_1();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("1538bc80-05de-4fd2-bd0a-684199eca017", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("1538bc80-05de-4fd2-bd0a-684199eca017", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_ClampVector3_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("45909fd1-2f81-4e86-bb8b-3d7b22bd4c0b", "CapsuleMovement", Relay_ClampVector3_1)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_target_1 = local_Axes_UnityEngine_Vector3;
               
            }
            {
            }
         }
         method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_1 = CapsuleMovement.ClampVector3(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_target_1);
         local_Axes_UnityEngine_Vector3 = method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_1;
         Relay_In_6();
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at CapsuleMovement.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_6()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("fdc92e32-ca68-4031-b00e-98dec1b422d0", "Get_Delta_Time", Relay_In_6)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_6, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_6, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_6);
         local_12_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_6;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_6.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Get Delta Time.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("900061e4-b1ee-4920-b73a-7feeddaa6fc2", "Multiply_Float", Relay_In_9)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_9 = local_scaleUnits_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_9 = local_12_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_9.In(logic_uScriptAct_MultiplyFloat_v2_A_9, logic_uScriptAct_MultiplyFloat_v2_B_9, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_9, out logic_uScriptAct_MultiplyFloat_v2_IntResult_9);
         local_17_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_9;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_9.Out;
         
         if ( test_0 == true )
         {
            Relay_In_15();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_15()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("fd06497d-8b8d-46f4-bf13-ebfefad2209d", "Multiply_Vector3_With_Float", Relay_In_15)) return; 
         {
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_15 = local_Axes_UnityEngine_Vector3;
               
            }
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_15 = local_17_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_15.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_15, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_15, out logic_uScriptAct_MultiplyVector3WithFloat_Result_15);
         local_24_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_15;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_15.Out;
         
         if ( test_0 == true )
         {
            Relay_RotateAroundLocal_21();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Multiply Vector3 With Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_RotateAroundLocal_21()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1953c240-2c82-43f7-96ca-44cc258ac303", "UnityEngine_Transform", Relay_RotateAroundLocal_21)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_axis_21 = local_24_UnityEngine_Vector3;
               
            }
            {
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_23.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.RotateAroundLocal(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_axis_21, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_angle_21);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:Axes", local_Axes_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "b12bda7f-fc0b-4f4a-b41e-70539031e73c", local_Axes_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:scaleUnits", local_scaleUnits_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "87417aad-5699-4e69-801d-e08ca9dd2165", local_scaleUnits_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:12", local_12_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "93d8381e-c803-437a-87dd-48b40bb59762", local_12_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:17", local_17_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "43629674-ead1-4601-ab0e-05a2f4928494", local_17_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:24", local_24_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7a086cae-79f9-44e3-b85a-413a9f372402", local_24_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}

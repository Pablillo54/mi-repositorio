﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class AgentToPoint : MonoBehaviour {

    public GameObject target;
    private NavMeshAgent navAgent;
    public bool followTarget, followFromStart;

    void Start()
    {
        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (target && followFromStart)
        {
            navAgent.SetDestination(target.transform.position);
        }
    }

    void Update()
    {
        if (target && followTarget)
        {
            navAgent.SetDestination(target.transform.position);
        }
    }
} 
